#include "QrEnc.h"

bool QREncode(const QByteArray &input, QImage &resultImage, unsigned char pixelSize, QRecLevel correctionLevel)
{
    QRcode *result = QRcode_encodeData(input.size(), (const unsigned char*)input.data(), 4, correctionLevel);

    if (result)
    {
        QImage qrimage(result->width*pixelSize, result->width*pixelSize, QImage::Format_RGB32);

        unsigned int y = 0;
        unsigned int x = 0;

        memset(qrimage.bits(), 0xFF, qrimage.byteCount());

        unsigned char *p = result->data;
        for(y=0; y<result->width; y++)
        {
            unsigned char *row = (p+(y*result->width));

            unsigned char pen = 0;
            unsigned int x0  = 0;
            for(x=0; x<result->width; x++)
            {
                if( !pen )
                {
                    pen = *(row+x)&0x1;
                    x0 = x;
                }
                else
                {
                    if(!(*(row+x)&0x1))
                    {
                        for (int e = 0; e<pixelSize; ++e)
                        {
                            memset((void*)(qrimage.bits()+((y*pixelSize + e )*result->width*pixelSize + x0*pixelSize)*4),0x0,4*(x-x0)*pixelSize);
                        }
                        pen = 0;
                    }
                }
            }
            if( pen )
            {
                for (int e = 0; e<pixelSize; ++e)
                {
                    memset((void*)(qrimage.bits()+((y*pixelSize + e )*result->width*pixelSize + x0*pixelSize)*4),0x0,4*(x-x0)*pixelSize);
                }
            }
        }

        resultImage = qrimage.copy();
        QRcode_free(result);

        return true;
    }
    return false;
}
