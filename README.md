![QQrEnc](https://bitbucket.org/futurapp/qqrenc/raw/7d0ce63a96577cff0ba95b8ed1f7ece813e1387a/logo.png)

QQrEnc
======

QQrEnc is a Qt wrapper for the library [qrencode](http://fukuchi.org/works/qrencode/index.html.en).

It provides an easy way to generate QR Code as QImage.

The only function of this library is

    [IN] input: the input data
    [OUT] resultImage: the output image
    [IN] pixelSize: the pixel size of the output QR Code image
    [IN] correctionLevel: the error correction level

    bool QREncode(const QByteArray &input, QImage &resultImage, unsigned char pixelSize = 3, QRecLevel correctionLevel = QR_ECLEVEL_M);

function will return true if the QR Code is generated successfully.

Example:
--------

    #include "QrEnc.h"

    int main()
    {
        QByteArray data;
        data.append(QString("Blah... Blah... Blah.."));
        QImage result;
        if (QREncode(data, result))
        {
            result.save("result.png");
        }
    }

The above example code will generate a result.png:

![QR Result](https://bitbucket.org/futurapp/qqrenc/raw/7d0ce63a96577cff0ba95b8ed1f7ece813e1387a/result.png)

License:
--------

QQrEnc.h and QQrEnc.cpp is under [WTFPL 2.0](http://www.wtfpl.net/) license.

       DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                        Version 2, December 2004 

     Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

     Everyone is permitted to copy and distribute verbatim or modified 
     copies of this license document, and changing it is allowed as long 
     as the name is changed. 

                DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

      0. You just DO WHAT THE FUCK YOU WANT TO.