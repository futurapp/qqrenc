#include <QCoreApplication>
#include <QImage>

#include "QrEnc.h"

int main()
{
    QByteArray data;
    data.append(QString("Blah... Blah... Blah.."));
    QImage result;
    if (QREncode(data, result))
    {
        result.save("result.png");
    }
}
