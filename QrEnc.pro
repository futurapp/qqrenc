#-------------------------------------------------
#
# Project created by QtCreator 2013-04-30T12:58:06
#
#-------------------------------------------------

QT       += core gui


TARGET = QrEnc
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    QrEnc.cpp \
    qrencode/split.c \
    qrencode/rscode.c \
    qrencode/qrspec.c \
    qrencode/qrinput.c \
    qrencode/qrencode.c \
    qrencode/mqrspec.c \
    qrencode/mmask.c \
    qrencode/mask.c \
    qrencode/bitstream.c

OTHER_FILES += \
    QrEnc.pro.user \
    QrEnc.pro

HEADERS += \
    QrEnc.h \
    qrencode/split.h \
    qrencode/rscode.h \
    qrencode/qrspec.h \
    qrencode/qrinput.h \
    qrencode/qrencode_inner.h \
    qrencode/qrencode.h \
    qrencode/mqrspec.h \
    qrencode/mmask.h \
    qrencode/mask.h \
    qrencode/bitstream.h
