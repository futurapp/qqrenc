#ifndef QRENC_H
#define QRENC_H

#include <QImage>

extern "C"
{
    #include "qrencode/qrencode.h"
}

bool QREncode(const QByteArray &input, QImage &resultImage, unsigned char pixelSize = 3, QRecLevel correctionLevel = QR_ECLEVEL_M);

#endif // QRENC_H
